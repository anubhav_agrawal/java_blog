import React, {Fragment} from "react";
import {useParams, Route} from "react-router-dom";


const BlogDetail=()=>{
    const params = useParams();
    return(
        <Fragment>
            <h1>Blog Detail</h1>
            <p>{params.blogId}</p>
            <Route path={`/blogs/${params.blogId}/comments`}>

            </Route>
        </Fragment>
    )
}

export default BlogDetail;