import React, {Fragment, useState} from "react";
import MainHeader from "../components/MainHeader/MainHeader";
import BlogList from "../components/blogs/BlogList";

const AllBlogs = () => {

    return (
        <Fragment>
            <main>
                <BlogList/>
            </main>
        </Fragment>
    )
}

export default AllBlogs;