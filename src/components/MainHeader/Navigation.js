import React, {Fragment, useState} from "react";
import AddBlog from "../../pages/AddBlog";
import classes from './Navigation.module.css'
import {Link} from "react-router-dom";
import icon from "../../resources/icon.png";

const Navigation = () => {

    const [isLoggedIn, setIsLoggedIn] = useState(false);

    const loginHandler = () => {
        setIsLoggedIn(true);
    };

    const logoutHandler = () => {
        setIsLoggedIn(false);
    };

    return (
        <nav className={classes.navbar}>
            <ul className={classes.left}>
                <div className={classes.navbarheader}>
                    <Link to="/" alt="">
                        <img src={icon}/>
                    </Link>
                    <Link to="/" className={classes.navbarbrand}>
                        top java blogs
                    </Link>
                </div>
                <Link to="/blogs" className={classes.navbarleft}>
                    Blogs
                </Link>

                <Link to="/news" className={classes.navbarleft}>
                    News
                </Link>
            </ul>

            <ul className={classes.right}>
                {isLoggedIn && (
                    <Fragment>
                        <Link to={AddBlog} className={classes.navbarleft}>Add Blog</Link>
                        <Link to="/blogs" className={classes.navbarleft}>Logout</Link>
                    </Fragment>
                )}
                {!isLoggedIn && (
                    <Fragment>
                        <Link to="/login" className={classes.navbarleft}>Login</Link>
                        <Link to="/login" className={classes.navbarleft}>Register</Link>
                    </Fragment>
                )}
            </ul>
        </nav>
    );
};

export default Navigation;