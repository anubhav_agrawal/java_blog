import './App.css';
import {Route, Switch, Redirect, NavLink} from 'react-router-dom'
import AllBlogs from "./pages/AllBlogs";
import BlogDetail from "./pages/BlogDetail";
import Login from "./components/Login/Login";
import Register from "./components/Login/Register";
import MainHeader from "./components/MainHeader/MainHeader";
import {Fragment} from "react";
import TopBlogs from "./pages/TopBlogs";
import Navigation from "./components/MainHeader/Navigation";

function App() {
  return (
      <Fragment>
        <Navigation/>
        <Switch>
          <Route path='/' exact>
            <TopBlogs/>
          </Route>
          <Route path='/blogs' exact>
            <AllBlogs/>
          </Route>
          <Route path='/blogs/:blogId'>
            <BlogDetail/>
          </Route>
          <Route path='/login'>
            <Login/>
          </Route>
          <Route path='/register'>
            <Register/>
          </Route>
        </Switch>
      </Fragment>
  );
}

export default App;
